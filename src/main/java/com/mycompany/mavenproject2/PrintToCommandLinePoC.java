package com.mycompany.mavenproject2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class PrintToCommandLinePoC{

  public final static int SOCKET_PORT = 12345;  
  public static String FILE_TO_SEND = "C:/Users/jstrah/Desktop";  
    
  public static void main(String[] args) throws IOException{
    FileInputStream fis = null;
    BufferedInputStream bis = null;
    OutputStream os = null;
    ServerSocket servsock = null;
    Socket sock = null;
    
    try {
      servsock = new ServerSocket(SOCKET_PORT);
      while (true) {
        System.out.println("Waiting...");
        try {
          sock = servsock.accept();
          System.out.println("Accepted connection : " + sock);
          // send file
          File myFile = new File (FILE_TO_SEND);
          byte [] mybytearray  = new byte [(int)myFile.length()];
          fis = new FileInputStream(myFile);
          bis = new BufferedInputStream(fis);
          bis.read(mybytearray,0,mybytearray.length);
          os = sock.getOutputStream();
          System.out.println("Sending " + FILE_TO_SEND + "(" + mybytearray.length + " bytes)");
          os.write(mybytearray,0,mybytearray.length);
          os.flush();
          System.out.println("Done.");
        }
        finally {
          if (bis != null) bis.close();
          if (os != null) os.close();
          if (sock!=null) sock.close();
        }
      }
    }
    finally {
      if (servsock != null) servsock.close();
    }
    
  }
  
  public void testPoC(){
    try{
      String[] commands = {"ls", "dir"};
      runTestCommands(commands);
    } catch(Exception e){
      e.printStackTrace();
      System.out.println(e.getMessage());
    }
  }
          
          
  private static void runTestCommands(String[] commands) throws IOException{
    for(int i = 0; i < commands.length; i++){
      Process process = Runtime.getRuntime().exec(commands[i]);
      printCommandOutput(process);
      printErrors(process);
      
      System.out.println("///////////////////////////////////////////////////////////");
    }
  }
  
  private static void printCommandOutput(Process process) throws IOException{
    String output; 
    BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
    System.out.println("Here is the standard output of the command:\n");
    while ((output = stdInput.readLine()) != null) {
      System.out.println(output);
    }
    System.out.println("Done\n");
  }
  
  private static void printErrors(Process process)throws IOException{
    String output; 
    BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
    System.out.println("Here is the standard error of the command (if any):\n");
    while ((output = stdError.readLine()) != null) {
      System.out.println(output);
    }
    System.out.println("Done\n");
  }
}