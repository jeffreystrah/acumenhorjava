package com.mycompany.mavenproject2;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.jsp.PageContext;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import org.springframework.stereotype.Controller;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class HelloController {
    FileConverter converterClass = new FileConverter();
    PrintToCommandLinePoC testClass = new PrintToCommandLinePoC();
        
    public HelloController() {
    }
    
    @RequestMapping(value="/sayhi", method=RequestMethod.GET)
    public String sayHi(Map<String, Object> model) {      
        String messageForJSP = "butts";
        model.put("message", messageForJSP );
        
        File file ;
        int maxFileSize = 5000 * 1024;
        int maxMemSize = 5000 * 1024;

        return "hello";
    }
    
    @RequestMapping(value="/convert", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public String convertTiff() throws IOException {      
        String[] commands = new String[3];
        converterClass.runMagick(commands);

        return "hello";
    }
    
    @RequestMapping(value="/testCommandLine", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public String testCommandLine() throws IOException {      
        testClass.testPoC();

        return "hello";
    }
}
