package com.mycompany.mavenproject2;

import java.io.*;

/**
 *
 * @author dmckinnon
 */


public class FileConverter {
    
    public static void main(String[] args) {
        try {
            String[] commands = {"magick", "Batman.tif", "BatmanConv.pdf"};
            runMagick(commands);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
}

    public static void runMagick(String[] commands) throws IOException{
    Process process = Runtime.getRuntime().exec(commands);
    printCommandOutput(process);
    printErrors(process);
  }
  
    private static void printCommandOutput(Process process) throws IOException {
        String output;
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
        System.out.println("Here is the standard output of the command:\n");
        while ((output = stdInput.readLine()) != null) {
            System.out.println(output);
        }
        System.out.println("Done\n");
    }

    private static void printErrors(Process process) throws IOException {
        String output;
        BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        System.out.println("Here is the standard error of the command (if any):\n");
        while ((output = stdError.readLine()) != null) {
            System.out.println(output);
        }
        System.out.println("Done\n");
    }
}
