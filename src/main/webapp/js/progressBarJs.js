$( document ).ready(function() {
    
    var counterNumber = 0;
    
    $( function() {
        var progressTimer,
          progressbar = $( "#progressbar" ),
          progressLabel = $( ".progress-label" ),
          dialogButtons = [{
            text: "Cancel Download",
            click: cancelDownload
          }],
      
          dialog = $( "#dialog" ).dialog({
            autoOpen: false,
            closeOnEscape: false,
            resizable: false,
            buttons: dialogButtons,
            open: function() {
              progressTimer = setTimeout( progress, 2000 );
            },
            beforeClose: function() {
              uploadButton.button( "option", {
                disabled: false,
                label: "Start Download"
              });
            }
          }),
          
          uploadButton = $( "#uploadButton" )
            .button()
            .on( "click", function(event) {
                event.preventDefault();
                beginConvertingFile();
              $( this ).button( "option", {
                disabled: true,
                label: "Downloading..."
              });
              dialog.dialog( "open" );
            });
            
          uploadToS3Button = $("#uploadToS3Button")
            .button()
            .on( "click", function(event) {
                event.preventDefault();
                testCommandLine();
              $( this ).button( "option", {
                disabled: true,
                label: "Testing. . ."
              });
              dialog.dialog( "open" );
            });
            
       progressbar.progressbar({
          value: false,
          change: function() {
            progressLabel.text( "Current Progress: " + progressbar.progressbar( "value" ) + "%" );
          },
          complete: function() {
            progressLabel.text( "Complete!" );
            dialog.dialog( "option", "buttons", [{
              text: "Close",
              click: closeDownload
            }]);
            $(".ui-dialog button").last().trigger( "focus" );
          }
        });

        function progress() {
          var val = progressbar.progressbar( "value" ) || 0;

          progressbar.progressbar( "value", val + Math.floor( Math.random() * 3 ) );

          if ( val <= 99 ) {
            progressTimer = setTimeout( progress, 50 );
          }
        }
        
        function beginConvertingFile(){
            $.ajax({
                method: 'POST',
                url: 'http://acumenhortestbean.us-east-1.elasticbeanstalk.com/convert',
                data:JSON.stringify({
                    programName: "imconvert",
                    fileLocation: $('#fileUploadSelect').val(),
                    saveLocation: 'C:\\Workspace\\PDFStorage'
                }),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                'dataType': 'text'
            }).success(function (data, status){
                $('#fileUploadSelect').val('');
                console.log("It was a success");
            }).error(function (data, status) {
                console.log("Where did I mess up?");
                console.log(status);
                console.log(data);
            });
        };
        
        function testCommandLine(){
            $.ajax({
                method: 'POST',
                url: 'http://acumenhortestbean.us-east-1.elasticbeanstalk.com/testCommandLine',
                data:JSON.stringify({
                    programName: "imconvert",
                    fileLocation: $('#fileUploadSelect').val(),
                    saveLocation: 'C:\\Workspace\\PDFStorage'
                }),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                'dataType': 'text'
            }).success(function (data, status){
                $('#fileUploadSelect').val('');
                console.log("Command line test did not throw exception");
            }).error(function (data, status) {
                console.log("Where did I mess up?");
                console.log(status);
                console.log(data);
            });
        }
        

        function closeDownload() {
          clearTimeout( progressTimer );
          dialog
            .dialog( "option", "buttons", dialogButtons )
            .dialog( "close" );
          progressbar.progressbar( "value", false );
          progressLabel
            .text( "Starting upload..." );
          uploadButton.trigger( "focus" );
          displayConversionStatus(true);
          $("#fileUploadSelect").val("");          
        }
        
        function cancelDownload(){
          clearTimeout( progressTimer );
          dialog
            .dialog( "option", "buttons", dialogButtons )
            .dialog( "close" );
          progressbar.progressbar( "value", false );
          progressLabel
            .text( "Starting upload..." );
          uploadButton.trigger( "focus" );
          $("#fileUploadSelect").val("");
          displayConversionStatus(false);
        }
        
        function displayConversionStatus(boolean){
            var responseText = $("#fileUploadSelect").val();
            var responseText2 = "The conversion was cancelled";
            var displayTable = $('#contentRows');
            
            if (boolean === true){
                displayTable.append($('<tr>')
                    .append($('<td>')
                            .append(responseText)
                    )
                );
            }else{
                displayTable.append($('<tr>')
                    .append($('<td>')
                            .append(responseText2)
                    )
                );
            };
        }    
    });
    
});