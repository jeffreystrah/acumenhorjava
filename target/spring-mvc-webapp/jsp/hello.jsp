<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Hello Controller Page</title>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>-->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/progressbarcss.css" rel="stylesheet">
   </head>
    <body>
        <div class="container">
            <h1>PDF Conversion and Backup:</h1>
            <hr/>
            <div class="navbar">
                 <ul class="nav nav-tabs">
                 <li role="presentation"><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/sayhi">Hello Controller</a></li>
                </ul>    
            </div>
            <div class="col-md-5">
                <h2>Tiff to PDF conversion:</h2>
                <br/>
                <form action="UploadServlet" method="post" enctype="multipart/form-data">
                    <input type="file" name="file" size="100" style="background-color: #DCDCDC; outline: 1px " id="fileUploadSelect"/>
                        <br />
                        <div style="width: 100%; text-align: center;">
                            <button type="button" class="btn btnp-rimary" id="uploadButton">Convert to PDF</button>
                        </div>
                </form>
                <br/><br/><br/><br/><br/><br/>
                <div id="dialog" title="File Conversion" style="background-color: #DCDCDC">
                    <div class="progress-label"></div>
                    <div id="progressbar"></div>
                </div>
                <div id="statusBox">
                    
                </div>
            </div>
                
            <div class="col-md-offset-1 col-md-6">
                <span>
                    <h2>Converted Files to Upload:</h2>
                </span>
                <table id="PdfTable" class="table table-hover">
                    <tr>
                        <th width="100%">File Name</th>
                    </tr>
                    <tbody id="contentRows"></tbody>
                </table>
                <button type="button" class="btn btnp-rimary" id="uploadToS3Button">Upload to S3</button>
            </div>
        </div>


        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>        
        <script src="${pageContext.request.contextPath}/js/progressBarJs.js"></script>
    </body>
</html>

